///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01a - Hello World - EE 205 - Spr 2022
///
/// @file    updateCats.c
/// @version 1.0 - Initial version
///
///
/// @author  Dane Sears dsears@hawaii.edu
/// @date    16_FEB_2022
///
////////////////////////////////////////////////////////////////////////////
#include<stdio.h>
#include<string.h>

#include "updateCats.h"
#include "catDatabase.h"

void updateCatName(int index, char newName[])
{
   if(strlen(newName) < 1 || strlen(newName) > MAX_NAME)
{
   printf("ERROR:\nCat name must be greater than 0 and less than 30.\nCat weight must be greater than zero\nThe number of Cats submiited into database\nmay not exceed 10\n");
}
for(int index; index < MAX_CATS; index++)
{
   if(strcmp(Name[index], newName) == 0)
   {
      printf("ERROR: This name is already in the database\n");
   //return void;
   }
}
//return void;
strcpy(Name[index], newName);
}

void fixCat(int index)
{
   if(isFixed[index] == 0)
   {
      isFixed[index] = true;
  //    printf("%s is NOT fixed.\n",Name[index]);
   }else{
      isFixed[index] = false;
    //  printf("%s is fixed.\n",Name[index]);
   }
}

void updateCatWeight(int index, float newWeight)
{
   if(newWeight <= 0)
   {
      printf("Weight must exceed zero.\n");
   }else{
      weight[index] = newWeight;
   }
   //return 0;
}

