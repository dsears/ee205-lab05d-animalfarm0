///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01a - Hello World - EE 205 - Spr 2022
///
/// @file    addCats.c
/// @version 1.0 - Initial version
///
///
/// @author  Dane Sears dsears@hawaii.edu
/// @date    16_FEB_2022
///
////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "addCats.h"
#include "catDatabase.h"

#define DEBUG

int validateDatabase(char name[],float catWeight) {
     //int i;
  if ((catWeight < 0) | (currentNumCats > 9) | (strlen(name) > MAX_NAME) | (strlen(name) == 0))
  {
  printf("ERROR:\nCat name must be greater than 0 and less than 30.\nCat weight must be greater than zero\nThe number of Cats submiited into database\nmay not exceed 10\n");
  return 0;
  }
  return 1;
  }

int addCat(char name[], enum gender catGender, enum breed catBreed, bool catFixed,float catWeight){

if (validateDatabase(name, catWeight))
{
strcpy(Name[currentNumCats],name);

   weight[currentNumCats] = catWeight;
   Gender[currentNumCats] = catGender;
   Breed[currentNumCats] = catBreed;
   isFixed[currentNumCats] = catFixed;
#ifdef DEBUG 
   printf("DEBUG: cat name is %c, cat gender is %d, cat breed is %d,\nis cat fixed?: %d, and cat weight is %.2f\n", Name[currentNumCats][0], Gender[currentNumCats], Breed[currentNumCats], isFixed[currentNumCats], weight[currentNumCats]);
#endif
}

   return currentNumCats++;
}

