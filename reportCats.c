///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01a - Hello World - EE 205 - Spr 2022
///
/// @file    reportCats.c
/// @version 1.0 - Initial version
///
///
/// @author  Dane Sears dsears@hawaii.edu
/// @date    16_FEB_2022
///
////////////////////////////////////////////////////////////////////////////
#include<stdio.h>
#include<string.h>
#include<stdbool.h>

#include "reportCats.h"
#include "catDatabase.h"
#include "addCats.h"

void printCat(int index){
   if (index < 0 || index > MAX_CATS){
printf("animalFarm0: Bad cat %d",index);
   }else{
     printf("cat index = %d  name = %s  gender = %d breed = %d  isFixed = %d  weight = %.2f\n", index, Name[index], Gender[index], Breed[index], isFixed[index], weight[index]);
 }
//   return void;
}

void printAllCats(){
    printf("The name(s) of the cat(s) in the database:\n");
  for (int index = 0; index < MAX_CATS; index++ )
   {
     printf("%s\n",Name[index]);
  }
 // return void;
}

int findCat(char name[]){
   for (int index = 0; index < MAX_CATS; index++)
   {
      if (strcmp(Name[index],name) == 0){
printf("%s is cat #%d\n",name,index);
   //return index;
            }
   }
   return 1;
}
//return void;
 
