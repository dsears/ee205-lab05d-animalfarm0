///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01a - Hello World - EE 205 - Spr 2022
///
/// @file    addCats.h
/// @version 1.0 - Initial version
///
///
/// @author  Dane Sears dsears@hawaii.edu
/// @date    16_FEB_2022
///
//////////////////////////////////////////////////////////////////////////////
#pragma once
#include "catDatabase.h"
     //int i;


extern int addCat(char name[], enum gender catGender, enum breed catBreed, bool catFixed,float catWeight);
