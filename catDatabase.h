///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01a - Hello World - EE 205 - Spr 2022
///
/// @file    catDatabase.h
/// @version 1.0 - Initial version
///
///
/// @author  Dane Sears dsears@hawaii.edu
/// @date    16_FEB_2022
///
////////////////////////////////////////////////////////////////////////////

#pragma once
#include<stdbool.h>
#define MAX_CATS 10
#define MAX_NAME 30

extern float weight[MAX_CATS];
extern bool isFixed[MAX_CATS];
extern int currentNumCats;
//int currentNumCats; //high-water mark for cats
enum gender {UNKOWN_GENDER, MALE, FEMALE};
enum breed {UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};
extern char name[MAX_CATS][MAX_NAME];
extern char Name[MAX_CATS][MAX_NAME];
extern enum gender Gender[MAX_CATS];
extern enum breed Breed[MAX_CATS];
