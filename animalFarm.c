///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01a - Hello World - EE 205 - Spr 2022
///
/// @file    animalFarm.c
/// @version 1.0 - Initial version
///
///
/// @author  Dane Sears dsears@hawaii.edu
/// @date    16_FEB_2022
///
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>

#include "catDatabase.h"
#include "addCats.h"
#include "reportCats.h"
#include "updateCats.h"
#include "deleteCats.h"

#define DEBUG //tests/shows usability of animalFarm

int main() {
   printf( "Starting Animal Farm 0\n" );
#ifdef DEBUG
   addCat( "Christina", FEMALE, MAINE_COON, false, 8.8 );
   addCat( "Dane", MALE, SPHYNX, false, 235);
   printAllCats(); //print all initial names

   printCat(1); //print Dane info

   findCat("Dane"); //find Dane's place in the index
   updateCatName(1, "Danus Torbalds");
 
   fixCat(0); //fixes christina
   printCat(0); //checks to see that she has been fixed

   printAllCats(); //for comfirming name changes
                   
   deleteAllCats(); 
   printCat(1); //confirms individual data has been deleted
   printAllCats(); //confirms all cats have been deleted
   printf( "Done with Animal Farm 0\n");
#endif
}
