///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01a - Hello World - EE 205 - Spr 2022
///
/// @file    deleteCats.c
/// @version 1.0 - Initial version
///
///
/// @author  Dane Sears dsears@hawaii.edu
/// @date    16_FEB_2022
///
///////////////////////////////////////////////////////////////////////////
#include<string.h>
#include "catDatabase.h"
#include "deleteCats.h"

void deleteAllCats()
{
   for(int i = 0; i < MAX_CATS; i++)
   {
      memset(Name[i], 0, MAX_CATS);
    //  memset(Gender[i], 0, MAX_CATS);
   }
   memset(Gender, 0, MAX_CATS*sizeof(Gender[0]));
   //Gender[MAX_CATS] = {0};
   memset(Breed, 0, MAX_CATS*sizeof(Breed[0]));
   memset(weight, 0, MAX_CATS*sizeof(weight[0]));
   memset(isFixed, false, MAX_CATS);
}
